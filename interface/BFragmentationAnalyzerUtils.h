#ifndef _BFragmentationAnalyzerUtils_h_
#define _BFragmentationAnalyzerUtils_h_

#include "DataFormats/JetReco/interface/GenJetCollection.h"
#include "DataFormats/HepMCCandidate/interface/GenParticleFwd.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

// For better c++, the previous macros have been replaced by inline functions.
inline bool IS_BHADRON_PDGID(int id) { return (abs(id)/100)%10 == 5 || (abs(id) >= 5000 && abs(id) <= 5999); }
inline bool IS_CHADRON_PDGID(int id) { return (abs(id)/100)%10 == 4 || (abs(id) >= 4000 && abs(id) <= 4999); }
inline bool IS_NEUTRINO_PDGID(int id) { return abs(id) == 12 || abs(id) == 14 || abs(id) == 16; }
inline bool IS_CHLEPTON_PDGID(int id) { return abs(id) == 11 || abs(id) == 13 || abs(id) == 15; }
inline bool IS_HEAVYCHLEPTON_PDGID(int id) { return abs(id) == 13 || abs(id) == 15; }

// Compile time optimizations
namespace Compile {
  constexpr float TAG_SCALE = 1e+20;
  constexpr bool DO_SUB = true;
  constexpr bool DO_ALL = true;
}

struct JetFragInfo_t {
  float xb_lead;
  float xb_lead_B;
  float xb_subLead;
  float xb_subLead_B;
  int leadTagId;
  int leadTagId_B;
  int subLeadTagId;
  int subLeadTagId_B;
  bool hasEleSemiLepDecay;
  bool hasMuSemiLepDecay;
  bool hasTauSemiLepDecay;
  int nbtags;
  int nctags;
  int ntautags;
};

JetFragInfo_t analyzeJet(const reco::GenJet &genJet, float tagScale = Compile::TAG_SCALE);

#endif
