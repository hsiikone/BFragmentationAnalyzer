#include <memory>
#include <string>
#include <map>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/stream/EDProducer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/FileInPath.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/StreamID.h"
#include "FWCore/Utilities/interface/Exception.h"
#include "DataFormats/Common/interface/Association.h"
#include "DataFormats/Common/interface/ValueMap.h"

#include "TopQuarkAnalysis/BFragmentationAnalyzer/interface/BFragmentationAnalyzerUtils.h"

#include "TFile.h"
#include "TGraph.h"
#include "TH2.h"

using std::string;
using std::vector;
using std::map;
using std::size_t;
using std::cout;
using std::endl;
using edm::ValueMap;

class BFragmentationWeightProducer : public edm::stream::EDProducer<> {
public:
  explicit BFragmentationWeightProducer(const edm::ParameterSet&);
  ~BFragmentationWeightProducer();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  virtual void beginStream(edm::StreamID) override;
  virtual void produce(edm::Event&, const edm::EventSetup&) override;
  virtual void endStream() override;

  template <typename S, typename T>
  void putValMap(edm::Event& iEt, S& jets, const string name, const vector<T> &vec);

  edm::EDGetTokenT<vector<reco::GenJet>> genJetsToken_;
  const vector<string> br_weights_;
  const vector<string> frag_weights_;
  const vector<string> frag_weights_vs_pt_;
  map<string, TGraph*> brWgtGr_;     // BR weights are stored as graphs
  map<string, TH2*> fragWgtPtHist_;  // frag weights vs. pt are stored as histograms
  map<string, TGraph*> fragWgtGr_;   // pt-averaged frag weights are stored as graphs

  map<string, vector<float>> jetWeights_;
  // The following are worth three floats, combined.
  const bool decayInfo_;
  vector<float> xbs_;
  vector<int> ids_;
  vector<bool> hasB_;
  vector<bool> hasC_;
  vector<bool> hasTau_;
  vector<bool> isSemiLeptBTau_;
  vector<bool> isSemiLeptBMu_;
  vector<bool> isSemiLeptBEle_;
};

//
BFragmentationWeightProducer::BFragmentationWeightProducer(const edm::ParameterSet& iConfig)
    : genJetsToken_(consumes<vector<reco::GenJet>>(iConfig.getParameter<edm::InputTag>("src"))),
      br_weights_(iConfig.getParameter<vector<string>>("br_weights")),
      frag_weights_(iConfig.getParameter<vector<string>>("frag_weights")),
      frag_weights_vs_pt_(iConfig.getParameter<vector<string>>("frag_weights_vs_pt")),
      decayInfo_(iConfig.getParameter<bool>("add_decayinfo"))
{
  edm::FileInPath fp = iConfig.getParameter<edm::FileInPath>("br_weight_file");
  TFile* fIn = TFile::Open(fp.fullPath().c_str());
  for (const auto& wgt : br_weights_) {
    produces<ValueMap<float>>(wgt);
    brWgtGr_[wgt] = static_cast<TGraph*>(fIn->Get(wgt.c_str()));
    if (!brWgtGr_[wgt]) {
      throw cms::Exception("ObjectNotFound")
          << "Could not load object " << wgt << " from " << fp.fullPath() << std::endl;
    }
    // Initialize the vectors
    jetWeights_[wgt] = vector<float>();
  }
  fIn->Close();

  fp = iConfig.getParameter<edm::FileInPath>("frag_weight_file");
  fIn = TFile::Open(fp.fullPath().c_str());
  for (const auto& wgt : frag_weights_) {
    produces<ValueMap<float>>(wgt);
    string grName = wgt + "_smooth";
    fragWgtGr_[wgt] = static_cast<TGraph*>(fIn->Get(grName.c_str()));
    if (!fragWgtGr_[wgt]) {
      throw cms::Exception("ObjectNotFound")
          << "Could not load object " << grName << " from " << fp.fullPath() << std::endl;
    }
    // Initialize the vectors
    jetWeights_[wgt] = vector<float>();
  }
  fIn->Close();

  fp = iConfig.getParameter<edm::FileInPath>("frag_weight_vs_pt_file");
  fIn = TFile::Open(fp.fullPath().c_str());
  for (const auto& wgt : frag_weights_vs_pt_) {
    produces<ValueMap<float>>(wgt + "VsPt");
    string histName = wgt + "_smooth";
    TH2* hist = static_cast<TH2*>(fIn->Get(histName.c_str()));
    if (!hist) {
      throw cms::Exception("ObjectNotFound")
          << "Could not load object " << histName << " from " << fp.fullPath() << std::endl;
    }
    fragWgtPtHist_[wgt] = hist;
    // Initialize the vectors
    jetWeights_[wgt + "VsPt"] = vector<float>();
  }

  // Avoid re-running analyzeJet by producing jinfo pieces when requested.
  if (decayInfo_) {
    produces<ValueMap<float>>("xb");
    produces<ValueMap<int>>("leadTagId");
    produces<ValueMap<bool>>("hasBTag");
    produces<ValueMap<bool>>("hasCTag");
    produces<ValueMap<bool>>("hasTauTag");
    produces<ValueMap<bool>>("hasSemiLeptBTau");
    produces<ValueMap<bool>>("hasSemiLeptBMu");
    produces<ValueMap<bool>>("hasSemiLeptBEle");
  }

  fIn->Close();
}

//
BFragmentationWeightProducer::~BFragmentationWeightProducer() {}

//
void BFragmentationWeightProducer::produce(edm::Event& iEvent, const edm::EventSetup& iSetup) {
  using namespace edm;
  edm::Handle<vector<reco::GenJet>> genJets;
  iEvent.getByToken(genJetsToken_, genJets);

  // Clearing the pre-defined vectors: cheaper than allocation.
  for (const auto& wgt: br_weights_) jetWeights_[wgt].clear();
  for (const auto& wgt: frag_weights_) jetWeights_[wgt].clear();
  for (const auto& wgt: frag_weights_vs_pt_) jetWeights_[wgt + "VsPt"].clear();
  if (decayInfo_) {
    xbs_.clear();
    ids_.clear();
    hasB_.clear();
    hasC_.clear();
    hasTau_.clear();
    isSemiLeptBTau_.clear();
    isSemiLeptBMu_.clear();
    isSemiLeptBEle_.clear();
  }

  for (auto &genJet : *genJets) {
    // Map the gen particles which are clustered in this jet
    JetFragInfo_t jinfo = analyzeJet(genJet);

    const int absBid{abs(jinfo.leadTagId_B)};
    const float xb{jinfo.xb_lead_B};
    // Evaluate the weight to an alternative fragmentation model (if a tag id is available)
    for (const auto& wgt: frag_weights_) {
      // Here we can use the bins above xb=1
      jetWeights_[wgt].push_back(absBid ? fragWgtGr_[wgt]->Eval(xb) : 1.);
    }

    const bool doVsPt = absBid && xb > -1. && xb < 1 && genJet.pt() >= 30;
    for (const auto& wgt: frag_weights_vs_pt_) {
      // Here always use weight=1 if xb>1 or if outside of pT range
      float weight = 1.;
      if (doVsPt) {
        TH2* hist = fragWgtPtHist_[wgt];
        size_t xb_bin = hist->GetXaxis()->FindBin(xb);
        size_t pt_bin = hist->GetYaxis()->FindBin(genJet.pt());
        weight = hist->GetBinContent(xb_bin, pt_bin);
      }
      jetWeights_[wgt + "VsPt"].push_back(weight);
    }

    const bool doBr{absBid == 511 || absBid == 521 || absBid == 531 || absBid == 5122};
    const bool isSL = doBr && (jinfo.hasEleSemiLepDecay || jinfo.hasMuSemiLepDecay || jinfo.hasTauSemiLepDecay);
    const int slBid{doBr ? (isSL ? absBid : -absBid) : 0};
    for (const auto& wgt: br_weights_) {
      jetWeights_[wgt].push_back(doBr ? brWgtGr_[wgt]->Eval(slBid) : 1.);
    }

    if (decayInfo_) {
      // If non-B info is provided and no B-match is found, give alt info for xb and id.
      const bool altB = Compile::DO_ALL && absBid == 0;
      // Fill
      xbs_.push_back(altB ? jinfo.xb_lead : xb);
      ids_.push_back(altB ? jinfo.leadTagId : absBid);
      hasB_.push_back(jinfo.nbtags > 1);
      hasC_.push_back(jinfo.nctags > 0);
      hasTau_.push_back(jinfo.ntautags > 0);
      isSemiLeptBTau_.push_back(jinfo.hasTauSemiLepDecay);
      isSemiLeptBMu_.push_back(jinfo.hasMuSemiLepDecay);
      isSemiLeptBEle_.push_back(jinfo.hasEleSemiLepDecay);
    }
  }

  // Put into event
  for (auto it : jetWeights_) {
    putValMap(iEvent, genJets, it.first, it.second);
  }

  if (decayInfo_) {
    putValMap(iEvent, genJets, "xb", xbs_);
    putValMap(iEvent, genJets, "leadTagId", ids_);
    putValMap(iEvent, genJets, "hasBTag", hasB_);
    putValMap(iEvent, genJets, "hasCTag", hasC_);
    putValMap(iEvent, genJets, "hasTauTag", hasTau_);
    putValMap(iEvent, genJets, "hasSemiLeptBTau", isSemiLeptBTau_);
    putValMap(iEvent, genJets, "hasSemiLeptBMu", isSemiLeptBMu_);
    putValMap(iEvent, genJets, "hasSemiLeptBEle", isSemiLeptBEle_);
  }
}

template <typename S, typename T>
void BFragmentationWeightProducer::putValMap(edm::Event& iEvent, S& jets, const string name, const vector<T> &vec)
{
  auto valMap = std::make_unique<ValueMap<T>>();
  typename ValueMap<T>::Filler filler(*valMap);
  filler.insert(jets, vec.begin(), vec.end());
  filler.fill();
  iEvent.put(std::move(valMap), name);
}

//
void BFragmentationWeightProducer::beginStream(edm::StreamID) {}

//
void BFragmentationWeightProducer::endStream() {}

//
void BFragmentationWeightProducer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(BFragmentationWeightProducer);
