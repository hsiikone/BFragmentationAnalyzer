#include "TopQuarkAnalysis/BFragmentationAnalyzer/interface/BFragmentationAnalyzerUtils.h"

//
JetFragInfo_t analyzeJet(const reco::GenJet& genJet, float tagScale) {
  //loop over the constituents to analyze the jet leading pT tag and the neutrinos
  std::vector<const reco::GenParticle*> jconst = genJet.getGenConstituents();
  std::vector<const reco::Candidate*> tagConst;
  std::vector<const reco::Candidate*> tagConst_B;
  std::vector<const reco::Candidate*> tag_eSemiLep;
  std::vector<const reco::Candidate*> tag_muSemiLep;
  std::vector<const reco::Candidate*> tag_tauSemiLep;

  // fill the jet info
  JetFragInfo_t jinfo;
  // HOX: these numbers can include multiple instances of the same particle => mostly for 0 vs. >0 discrimination.
  jinfo.nbtags = 0;
  jinfo.nctags = 0;
  jinfo.ntautags = 0;

  for (size_t ijc = 0; ijc < jconst.size(); ijc++) {
    const reco::GenParticle* par = jconst[ijc];
    // Accepting stable (e/mu/nu status: 1) and decayed (tau/hadron status: 2) particles.
    if (par->status() != 2 && par->status() != 1) continue;

    int absid = abs(par->pdgId());
    const bool isTau{absid == 16 || absid == 15};
    if (isTau) ++jinfo.ntautags;

    // Definition: exclusive semileptonic =>  we seek for a lepton with a B hadron as a parent.
    if (IS_NEUTRINO_PDGID(absid) || IS_CHLEPTON_PDGID(absid)) {
      const reco::Candidate* leptonMother = par->mother();
      // The mother of this lepton is a status-2 B hadron -> store the hadron
      if (leptonMother && leptonMother->status() == 2 && IS_BHADRON_PDGID(leptonMother->pdgId())) {
        if (isTau) tag_tauSemiLep.push_back(leptonMother);
        else {
          if (absid == 14 || absid == 13) tag_muSemiLep.push_back(leptonMother);
          else tag_eSemiLep.push_back(leptonMother);
        }
      }
      // No further use for leptons
      continue;
    }

    // Stable particles no longer needed, look for decayed (b/c) hadrons.
    if (par->status() != 2) continue;

    // Count number of tags (unfortunately, the same parton may be repeated in the decay chain).
    // Hence, these numbers cannot be used as solid indicators of e.g. gluon splitting.
    if (IS_CHADRON_PDGID(absid)) ++jinfo.nctags;
    if (IS_BHADRON_PDGID(absid)) {
      ++jinfo.nbtags;
      // to find leading B hadron tag particle
      tagConst_B.push_back(par);
    }

    // to find leading tag particle, regardless of flavour
    if (Compile::DO_ALL) tagConst.push_back(par);
  }

  auto sortByDecrPt = [](const reco::Candidate* a, const reco::Candidate* b) { return a->pt() > b->pt(); };
  if (Compile::DO_ALL) std::sort(tagConst.begin(), tagConst.end(), sortByDecrPt);
  std::sort(tagConst_B.begin(), tagConst_B.end(), sortByDecrPt);

  const bool DoLead{Compile::DO_ALL && tagConst.size() > 0};
  const bool DoSub{Compile::DO_ALL && Compile::DO_SUB && tagConst.size() > 1};
  // filling only if the leading ghost hadron in the jet, regardless of flavour (this is not used anymore for fragmentation weights)
  jinfo.xb_lead = DoLead ? (tagConst[0]->pt() * tagScale) / genJet.pt() : -1;
  jinfo.leadTagId = DoLead ? tagConst[0]->pdgId() : 0;
  jinfo.xb_subLead = DoSub ? (tagConst[1]->pt() * tagScale) / genJet.pt() : -1;
  jinfo.subLeadTagId = DoSub ? tagConst[1]->pdgId() : 0;

  const bool DoLeadB{tagConst_B.size() > 0};
  const bool DoSubB{Compile::DO_SUB && tagConst_B.size() > 1};
  // Filled with leading B ghost hadron in the jet (even if there is another hadron with higher pt).
  jinfo.xb_lead_B = DoLeadB ? (tagConst_B[0]->pt() * tagScale) / genJet.pt() : -1;
  jinfo.leadTagId_B = DoLeadB ? tagConst_B[0]->pdgId() : 0;
  jinfo.xb_subLead_B = DoSubB ? (tagConst_B[1]->pt() * tagScale) / genJet.pt() : -1;
  jinfo.subLeadTagId_B = DoSubB ? tagConst_B[1]->pdgId() : 0;

  // tag the jet as having a semilep B decay if leading B-had is in the list of SL-decaying B-hads.
  jinfo.hasEleSemiLepDecay = false;
  jinfo.hasMuSemiLepDecay = false;
  jinfo.hasTauSemiLepDecay = false;
  if (tagConst_B.size() > 0) {
    const auto& b = tagConst_B.at(0);
    jinfo.hasEleSemiLepDecay = std::find(tag_eSemiLep.begin(), tag_eSemiLep.end(), b) != tag_eSemiLep.end();
    jinfo.hasMuSemiLepDecay = std::find(tag_muSemiLep.begin(), tag_muSemiLep.end(), b) != tag_muSemiLep.end();
    jinfo.hasTauSemiLepDecay = std::find(tag_tauSemiLep.begin(), tag_tauSemiLep.end(), b) != tag_tauSemiLep.end();
  }

  return jinfo;
}
